package id.ac.ui.cs.advprog.tutorial0.service;

import java.util.List;
import id.ac.ui.cs.advprog.tutorial0.model.Student;

public interface StudentService {

    public Student create(Student student);
    public List<Student> findAll();
}
